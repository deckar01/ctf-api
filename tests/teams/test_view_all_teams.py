from flask import json
from tests.base_case import BaseCase


class ViewAllTeamsCase(BaseCase):
    def setUp(self):
        super().setUp()
        self.disable(self.b_team)

    def test_view_all_teams_as_public(self):
        response = self.app.get('/teams/')
        self.assertEqual(response.status, '200 OK')
        teams = json.loads(response.data)
        self.assertEqual(len(teams), 1)
        keys = set(['name', 'actions'])
        self.assertEqual(set(teams[0].keys()), keys)
        self.assertEqual(teams[0]['name'], 'A Team')
        self.assertEqual(teams[0]['actions'], {'view?': '/teams/a-team/'})

    def test_view_all_teams_as_internal(self):
        self.login(self.alice)
        response = self.app.get('/teams/')
        self.assertEqual(response.status, '200 OK')
        teams = json.loads(response.data)
        self.assertEqual(len(teams), 1)
        keys = set(['name', 'actions'])
        self.assertEqual(set(teams[0].keys()), keys)
        self.assertEqual(teams[0]['name'], 'A Team')
        self.assertEqual(teams[0]['actions'], {'view?': '/teams/a-team/'})

    def test_view_all_teams_as_member(self):
        self.login(self.jared)
        response = self.app.get('/teams/')
        self.assertEqual(response.status, '200 OK')
        teams = json.loads(response.data)
        self.assertEqual(len(teams), 1)
        keys = set(['name', 'actions'])
        self.assertEqual(set(teams[0].keys()), keys)
        self.assertEqual(teams[0]['name'], 'A Team')
        self.assertEqual(teams[0]['actions'], {'view?': '/teams/a-team/'})

    def test_view_all_teams_as_admin(self):
        self.login(self.admin)
        response = self.app.get('/teams/')
        self.assertEqual(response.status, '200 OK')
        teams = json.loads(response.data)
        self.assertEqual(len(teams), 2)
        keys = set(['id', 'enabled', 'date_created', 'name', 'actions'])
        self.assertEqual(set(teams[0].keys()), keys)
        self.assertEqual(set(teams[1].keys()), keys)
        self.assertEqual(teams[0]['id'], 1)
        self.assertEqual(teams[0]['enabled'], True)
        self.assertEqual(teams[0]['name'], 'A Team')
        self.assertEqual(teams[0]['actions'], {
            'view?': '/teams/a-team/',
            'disable!': '/teams/a-team/disable/',
        })
        self.assertEqual(teams[1]['id'], 2)
        self.assertEqual(teams[1]['enabled'], False)
        self.assertEqual(teams[1]['name'], 'B Team')
        self.assertEqual(teams[1]['actions'], {
            'view?': '/teams/b-team/',
            'enable!': '/teams/b-team/enable/',
        })
