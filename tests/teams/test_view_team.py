from flask import json
from tests.base_case import BaseCase


class ViewTeamCase(BaseCase):
    def test_view_team_as_public(self):
        response = self.app.get('/teams/a-team/')
        self.assertEqual(response.status, '200 OK')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['name', 'members']))
        self.assertEqual(team['name'], 'A Team')
        self.assertEqual(team['members'], [{
            'username': 'jared',
            'actions': {'view?': '/users/jared/'},
        }])

    def test_view_team_as_internal(self):
        self.login(self.alice)
        response = self.app.get('/teams/a-team/')
        self.assertEqual(response.status, '200 OK')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['name', 'members']))
        self.assertEqual(team['name'], 'A Team')
        self.assertEqual(team['members'], [{
            'username': 'jared',
            'actions': {'view?': '/users/jared/'},
        }])

    def test_view_team_as_member(self):
        self.login(self.jared)
        response = self.app.get('/teams/a-team/')
        self.assertEqual(response.status, '200 OK')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['name', 'members', 'invitations']))
        self.assertEqual(team['name'], 'A Team')
        self.assertEqual(team['invitations'], [])
        self.assertEqual(team['members'], [{
            'username': 'jared',
            'actions': {'view?': '/users/jared/'},
        }])

    def test_view_team_as_admin(self):
        self.login(self.admin)
        response = self.app.get('/teams/a-team/')
        self.assertEqual(response.status, '200 OK')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set([
            'id', 'enabled', 'date_created', 'actions',
            'name', 'members', 'invitations',
        ]))
        self.assertEqual(team['id'], 1)
        self.assertEqual(team['enabled'], True)
        self.assertEqual(team['name'], 'A Team')
        self.assertEqual(team['invitations'], [])
        self.assertEqual(team['members'], [{
            'username': 'jared',
            'actions': {'view?': '/users/jared/'},
        }])
        self.assertEqual(team['actions'], {
            'disable!': '/teams/a-team/disable/',
        })

    def test_view_disabled_team_as_admin(self):
        self.disable(self.b_team)
        self.login(self.admin)
        response = self.app.get('/teams/b-team/')
        self.assertEqual(response.status, '200 OK')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set([
            'id', 'enabled', 'date_created', 'actions',
            'name', 'members', 'invitations',
        ]))
        self.assertEqual(team['id'], 2)
        self.assertEqual(team['enabled'], False)
        self.assertEqual(team['name'], 'B Team')
        self.assertEqual(team['members'], [])
        self.assertEqual(team['invitations'], [])
        self.assertEqual(team['actions'], {
            'enable!': '/teams/b-team/enable/',
        })

    def test_view_disabled_team_as_public(self):
        self.disable(self.b_team)
        response = self.app.get('/teams/b-team/')
        self.assertEqual(response.status, '404 NOT FOUND')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_found')
        self.assertEqual(team['message'], 'Team not found.')

    def test_view_unknown_team_as_public(self):
        response = self.app.get('/teams/g-team/')
        self.assertEqual(response.status, '404 NOT FOUND')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_found')
        self.assertEqual(team['message'], 'Team not found.')
