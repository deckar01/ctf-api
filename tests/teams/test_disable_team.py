from flask import json
from unittest.mock import patch
from tests.base_case import BaseCase


class DisableTeamCase(BaseCase):
    def test_disable_team_as_admin(self):
        self.login(self.admin)
        response = self.app.post('/teams/a-team/disable/')
        self.assertEqual(response.status, '200 OK')
        team = json.loads(response.data)
        assert 'enabled' in team.keys()
        assert 'actions' in team.keys()
        self.assertEqual(team['enabled'], False)
        self.assertEqual(team['actions'], {
            'enable!': '/teams/a-team/enable/',
        })

    def test_disable_team_as_public(self):
        response = self.app.post('/teams/a-team/disable/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_allowed')
        self.assertEqual(team['message'], 'Permission denied.')

    def test_disable_team_as_internal(self):
        self.login(self.alice)
        response = self.app.post('/teams/a-team/disable/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_allowed')
        self.assertEqual(team['message'], 'Permission denied.')

    def test_disable_team_as_member(self):
        self.login(self.jared)
        response = self.app.post('/teams/a-team/disable/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_allowed')
        self.assertEqual(team['message'], 'Permission denied.')

    @patch('ctf_api.models.session_manager.SessionManager.session.commit')
    def test_disable_team_with_unexpected_error(self, commit):
        self.login(self.admin)
        commit.side_effect = Exception('Unexpected')
        response = self.app.post('/teams/a-team/disable/')
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_working')
        self.assertEqual(team['message'], 'Something went wrong.')
