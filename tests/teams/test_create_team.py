from flask import json
from unittest.mock import patch
from tests.base_case import BaseCase
from ctf_api.config import Config
from ctf_api.models.team.team import Team


class CreateTeamCase(BaseCase):
    def setUp(self):
        super().setUp()
        self.voucher = self.create_team_voucher('buyme@test.com')
        self.team_x_data = {
            'name': 'Team X',
        }

    def tearDown(self):
        super().tearDown()
        Config.REQUIRE_TEAM_VOUCHER = True

    def test_create_team_as_admin(self):
        self.login(self.admin)
        response = self.app.post('/teams/', data=self.team_x_data)
        self.assertEqual(response.status, '200 OK')
        team = json.loads(response.data)
        self.assertEqual(
            set(team.keys()),
            set(['name', 'actions', 'members', 'id', 'enabled', 'date_created', 'invitations'])
        )
        self.assertEqual(team['name'], 'Team X')
        self.assertEqual(team['id'], 3)
        self.assertEqual(team['enabled'], True)
        self.assertEqual(team['members'], [])
        self.assertEqual(team['invitations'], [])
        self.assertEqual(team['actions'], {
            'disable!': '/teams/team-x/disable/',
        })
        self.assertEqual(self.admin.team, None)

    def test_create_team_with_missing_args(self):
        self.login(self.admin)
        response = self.app.post('/teams/', data={})
        self.assertEqual(response.status, '400 BAD REQUEST')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_valid')
        self.assertEqual(team['message'], {
            'name': ['Missing data for required field.'],
        })

    def test_create_team_as_public(self):
        response = self.app.post('/teams/', data=self.team_x_data)
        self.assertEqual(response.status, '403 FORBIDDEN')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_allowed')
        self.assertEqual(team['message'], 'You must be logged in to create a team.')

    def test_create_team_with_voucher(self):
        self.login(self.alice)
        self.team_x_data['voucher'] = self.voucher.token
        response = self.app.post('/teams/', data=self.team_x_data)
        self.assertEqual(response.status, '200 OK')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['name', 'members', 'invitations']))
        self.assertEqual(team['name'], 'Team X')
        self.assertEqual(team['members'], [
            {'username': 'alice', 'actions': {'view?': '/users/alice/'}},
        ])
        self.assertEqual(team['invitations'], [])
        self.assertEqual(self.voucher.used, True)
        self.assertEqual(self.alice.team.name, 'Team X')

    def test_create_team_with_missing_voucher(self):
        self.login(self.alice)
        response = self.app.post('/teams/', data=self.team_x_data)
        self.assertEqual(response.status, '403 FORBIDDEN')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_allowed')
        self.assertEqual(team['message'], 'You need a voucher to create a team.')

    def test_create_team_with_fake_voucher(self):
        self.login(self.alice)
        self.team_x_data['voucher'] = 'IC4NH4Z4CC355?'
        response = self.app.post('/teams/', data=self.team_x_data)
        self.assertEqual(response.status, '404 NOT FOUND')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_found')
        self.assertEqual(team['message'], 'Voucher not found.')

    def test_create_team_with_used_voucher(self):
        self.login(self.alice)
        self.voucher.used = True
        self.session.add(self.voucher)
        self.session.commit()
        self.team_x_data['voucher'] = self.voucher.token
        response = self.app.post('/teams/', data=self.team_x_data)
        self.assertEqual(response.status, '404 NOT FOUND')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_found')
        self.assertEqual(team['message'], 'Voucher not found.')

    def test_create_team_without_voucher(self):
        self.login(self.alice)
        Config.REQUIRE_TEAM_VOUCHER = False
        response = self.app.post('/teams/', data=self.team_x_data)
        self.assertEqual(response.status, '200 OK')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['name', 'members', 'invitations']))
        self.assertEqual(team['name'], 'Team X')
        self.assertEqual(team['members'], [
            {'username': 'alice', 'actions': {'view?': '/users/alice/'}},
        ])
        self.assertEqual(team['invitations'], [])
        self.assertEqual(self.alice.team.name, 'Team X')

    def test_create_team_with_duplicate_name(self):
        self.login(self.alice)
        self.team_x_data['voucher'] = self.voucher.token
        self.team_x_data['name'] = 'A Team'
        response = self.app.post('/teams/', data=self.team_x_data)
        self.assertEqual(response.status, '400 BAD REQUEST')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_valid')
        self.assertEqual(team['message'], 'That team name is already taken.')

    @patch('ctf_api.models.session_manager.SessionManager.session.commit')
    def test_create_team_with_unexpected_error(self, commit):
        self.login(self.alice)
        commit.side_effect = Exception('Unexpected')
        self.team_x_data['voucher'] = self.voucher.token
        response = self.app.post('/teams/', data=self.team_x_data)
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')
        team = json.loads(response.data)
        self.assertEqual(set(team.keys()), set(['status', 'message']))
        self.assertEqual(team['status'], 'not_working')
        self.assertEqual(team['message'], 'Something went wrong.')
