from flask import json
from tests.base_case import BaseCase


class LoginCase(BaseCase):
    def test_login(self):
        response = self.app.post('/login/', data={
            'username': 'jared',
            'password': 'password',
        })
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'success')
        self.assertEqual(user['message'], 'You are logged in.')
        with self.app.session_transaction() as session:
            self.assertEqual(session.get('user_id'), 2)

    def test_login_with_wrong_password(self):
        response = self.app.post('/login/', data={
            'username': 'alice',
            'password': 'goodguesser',
        })
        self.assertEqual(response.status, '403 FORBIDDEN')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_allowed')
        self.assertEqual(user['message'], 'Incorrect password.')
        with self.app.session_transaction() as session:
            self.assertEqual(session.get('user_id'), None)

    def test_login_as_unknown_user(self):
        response = self.app.post('/login/', data={
            'username': 'bob',
            'password': 'randomstranger',
        })
        self.assertEqual(response.status, '404 NOT FOUND')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_found')
        self.assertEqual(user['message'], 'User not found.')
        with self.app.session_transaction() as session:
            self.assertEqual(session.get('user_id'), None)

    def test_login_with_missing_args(self):
        response = self.app.post('/login/')
        self.assertEqual(response.status, '400 BAD REQUEST')
        status = json.loads(response.data)
        self.assertEqual(set(status.keys()), set(['status', 'message']))
        self.assertEqual(status['status'], 'not_valid')
        self.assertEqual(status['message'], {
            'username': ['Missing data for required field.'],
            'password': ['Missing data for required field.'],
        })
        with self.app.session_transaction() as session:
            self.assertEqual(session.get('user_id'), None)
