from flask import json, session
from tests.base_case import BaseCase


class LogoutCase(BaseCase):
    def test_logout(self):
        self.login(self.jared)
        response = self.app.post('/logout/')
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'success')
        self.assertEqual(user['message'], 'You are logged out.')
        with self.app.session_transaction() as session:
            self.assertEqual(session.get('user_id'), None)

    def test_logout_when_already_logged_out(self):
        response = self.app.post('/logout/')
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'success')
        self.assertEqual(user['message'], 'You are logged out.')
        with self.app.session_transaction() as session:
            self.assertEqual(session.get('user_id'), None)
