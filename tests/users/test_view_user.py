from flask import json
from tests.base_case import BaseCase


class ViewUserCase(BaseCase):
    def test_view_user_as_public(self):
        response = self.app.get('/users/jared/')
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['username', 'first_name', 'last_name', 'team']))
        self.assertEqual(user['username'], 'jared')
        self.assertEqual(user['first_name'], 'Jared')
        self.assertEqual(user['last_name'], 'Deckard')
        self.assertEqual(user['team'], {
            'name': 'A Team',
            'actions': {'view?': '/teams/a-team/'},
        })

    def test_view_user_as_internal(self):
        self.login(self.alice)
        response = self.app.get('/users/jared/')
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['username', 'first_name', 'last_name', 'team']))
        self.assertEqual(user['username'], 'jared')
        self.assertEqual(user['first_name'], 'Jared')
        self.assertEqual(user['last_name'], 'Deckard')
        self.assertEqual(user['team'], {
            'name': 'A Team',
            'actions': {'view?': '/teams/a-team/'},
        })

    def test_view_user_as_owner(self):
        self.login(self.jared)
        response = self.app.get('/users/jared/')
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(
            set(user.keys()),
            set([
                'username', 'first_name', 'last_name', 'email_address',
                'team', 'team_invitations',
            ])
        )
        self.assertEqual(user['username'], 'jared')
        self.assertEqual(user['first_name'], 'Jared')
        self.assertEqual(user['last_name'], 'Deckard')
        self.assertEqual(user['email_address'], 'jared@test.com')
        self.assertEqual(user['team'], {
            'name': 'A Team',
            'actions': {'view?': '/teams/a-team/'},
        })
        self.assertEqual(user['team_invitations'], [])

    def test_view_user_as_admin(self):
        self.login(self.admin)
        response = self.app.get('/users/jared/')
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(
            set(user.keys()),
            set([
                'username', 'first_name', 'last_name', 'email_address',
                'actions', 'id', 'enabled', 'date_created', 'is_admin',
                'team', 'team_invitations',
            ])
        )
        self.assertEqual(user['username'], 'jared')
        self.assertEqual(user['first_name'], 'Jared')
        self.assertEqual(user['last_name'], 'Deckard')
        self.assertEqual(user['email_address'], 'jared@test.com')
        self.assertEqual(user['id'], 2)
        self.assertEqual(user['enabled'], True)
        self.assertEqual(user['is_admin'], False)
        self.assertEqual(user['actions'], {
            'disable!': '/users/jared/disable/',
        })
        self.assertEqual(user['team'], {
            'name': 'A Team',
            'actions': {'view?': '/teams/a-team/'},
        })
        self.assertEqual(user['team_invitations'], [])

    def test_view_disabled_user_as_admin(self):
        self.disable(self.jared)
        self.login(self.admin)
        response = self.app.get('/users/jared/')
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(
            set(user.keys()),
            set([
                'username', 'first_name', 'last_name', 'email_address',
                'actions', 'id', 'enabled', 'date_created', 'is_admin',
                'team', 'team_invitations',
            ])
        )
        self.assertEqual(user['username'], 'jared')
        self.assertEqual(user['first_name'], 'Jared')
        self.assertEqual(user['last_name'], 'Deckard')
        self.assertEqual(user['email_address'], 'jared@test.com')
        self.assertEqual(user['id'], 2)
        self.assertEqual(user['enabled'], False)
        self.assertEqual(user['is_admin'], False)
        self.assertEqual(user['actions'], {
            'enable!': '/users/jared/enable/',
        })
        self.assertEqual(user['team'], {
            'name': 'A Team',
            'actions': {'view?': '/teams/a-team/'},
        })
        self.assertEqual(user['team_invitations'], [])

    def test_view_disabled_user_as_public(self):
        self.disable(self.jared)
        response = self.app.get('/users/jared/')
        self.assertEqual(response.status, '404 NOT FOUND')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_found')
        self.assertEqual(user['message'], 'User not found.')

    def test_view_unknown_user(self):
        self.disable(self.jared)
        response = self.app.get('/users/bob/')
        self.assertEqual(response.status, '404 NOT FOUND')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_found')
        self.assertEqual(user['message'], 'User not found.')

    def test_view_user_with_disabled_team(self):
        self.disable(self.a_team)
        response = self.app.get('/users/jared/')
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['username', 'first_name', 'last_name', 'team']))
        self.assertEqual(user['username'], 'jared')
        self.assertEqual(user['first_name'], 'Jared')
        self.assertEqual(user['last_name'], 'Deckard')
        self.assertEqual(user['team'], None)
