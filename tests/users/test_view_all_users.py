from flask import json
from tests.base_case import BaseCase


class ViewAllUsersCase(BaseCase):
    def test_view_all_users_as_public(self):
        response = self.app.get('/users/')
        self.assertEqual(response.status, '200 OK')
        users = json.loads(response.data)
        self.assertEqual(len(users), 3)
        keys = set(['username', 'first_name', 'last_name', 'actions', 'team'])
        self.assertEqual(set(users[0].keys()), keys)
        self.assertEqual(set(users[1].keys()), keys)
        self.assertEqual(users[0]['username'], 'admin')
        self.assertEqual(users[0]['first_name'], 'Max')
        self.assertEqual(users[0]['last_name'], 'Powers')
        self.assertEqual(users[0]['actions'], {'view?': '/users/admin/'})
        self.assertEqual(users[0]['team'], None)
        self.assertEqual(users[1]['username'], 'jared')
        self.assertEqual(users[1]['first_name'], 'Jared')
        self.assertEqual(users[1]['last_name'], 'Deckard')
        self.assertEqual(users[1]['actions'], {'view?': '/users/jared/'})
        self.assertEqual(users[1]['team'], {
            'name': 'A Team',
            'actions': {'view?': '/teams/a-team/'},
        })
        self.assertEqual(users[2]['username'], 'alice')
        self.assertEqual(users[2]['first_name'], 'Alice')
        self.assertEqual(users[2]['last_name'], 'Serry')
        self.assertEqual(users[2]['actions'], {'view?': '/users/alice/'})
        self.assertEqual(users[2]['team'], None)

    def test_view_all_users_as_internal(self):
        self.login(self.alice)
        response = self.app.get('/users/')
        self.assertEqual(response.status, '200 OK')
        users = json.loads(response.data)
        self.assertEqual(len(users), 3)
        keys = set(['username', 'first_name', 'last_name', 'actions', 'team'])
        self.assertEqual(set(users[1].keys()), keys)
        self.assertEqual(users[1]['username'], 'jared')
        self.assertEqual(users[1]['first_name'], 'Jared')
        self.assertEqual(users[1]['last_name'], 'Deckard')
        self.assertEqual(users[1]['actions'], {'view?': '/users/jared/'})
        self.assertEqual(users[1]['team'], {
            'name': 'A Team',
            'actions': {'view?': '/teams/a-team/'},
        })

    def test_view_all_users_as_owner(self):
        self.login(self.jared)
        response = self.app.get('/users/')
        self.assertEqual(response.status, '200 OK')
        users = json.loads(response.data)
        self.assertEqual(len(users), 3)
        keys = set(['username', 'first_name', 'last_name', 'actions', 'team'])
        self.assertEqual(set(users[1].keys()), keys)
        self.assertEqual(users[1]['username'], 'jared')
        self.assertEqual(users[1]['first_name'], 'Jared')
        self.assertEqual(users[1]['last_name'], 'Deckard')
        self.assertEqual(users[1]['actions'], {'view?': '/users/jared/'})
        self.assertEqual(users[1]['team'], {
            'name': 'A Team',
            'actions': {'view?': '/teams/a-team/'},
        })

    def test_view_all_users_as_admin(self):
        self.disable(self.jared)
        self.login(self.admin)
        response = self.app.get('/users/')
        self.assertEqual(response.status, '200 OK')
        users = json.loads(response.data)
        self.assertEqual(len(users), 3)
        keys = set([
            'username', 'first_name', 'last_name', 'email_address',
            'actions', 'id', 'enabled', 'date_created', 'is_admin', 'team',
        ])
        self.assertEqual(set(users[0].keys()), keys)
        self.assertEqual(set(users[1].keys()), keys)
        self.assertEqual(users[0]['username'], 'admin')
        self.assertEqual(users[0]['first_name'], 'Max')
        self.assertEqual(users[0]['last_name'], 'Powers')
        self.assertEqual(users[0]['email_address'], 'admin@test.com')
        self.assertEqual(users[0]['id'], 1)
        self.assertEqual(users[0]['enabled'], True)
        self.assertEqual(users[0]['is_admin'], True)
        self.assertEqual(users[0]['actions'], {
            'view?': '/users/admin/',
            'disable!': '/users/admin/disable/',
        })
        self.assertEqual(users[0]['team'], None)
        self.assertEqual(users[1]['username'], 'jared')
        self.assertEqual(users[1]['first_name'], 'Jared')
        self.assertEqual(users[1]['last_name'], 'Deckard')
        self.assertEqual(users[1]['email_address'], 'jared@test.com')
        self.assertEqual(users[1]['id'], 2)
        self.assertEqual(users[1]['enabled'], False)
        self.assertEqual(users[1]['is_admin'], False)
        self.assertEqual(users[1]['actions'], {
            'view?': '/users/jared/',
            'enable!': '/users/jared/enable/',
        })
        self.assertEqual(users[1]['team'], {
            'name': 'A Team',
            'actions': {'view?': '/teams/a-team/'},
        })
