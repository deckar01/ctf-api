from flask import json
from unittest.mock import patch
from tests.base_case import BaseCase


class EnableUserCase(BaseCase):
    def test_enable_user_as_admin(self):
        self.disable(self.jared)
        self.login(self.admin)
        response = self.app.post('/users/jared/enable/')
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        assert 'enabled' in user.keys()
        assert 'actions' in user.keys()
        self.assertEqual(user['enabled'], True)
        self.assertEqual(user['actions'], {
            'disable!': '/users/jared/disable/',
        })

    def test_enable_user_as_public(self):
        self.disable(self.jared)
        response = self.app.post('/users/jared/enable/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_allowed')
        self.assertEqual(user['message'], 'Permission denied.')

    def test_enable_user_as_internal(self):
        self.disable(self.jared)
        self.login(self.alice)
        response = self.app.post('/users/jared/enable/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_allowed')
        self.assertEqual(user['message'], 'Permission denied.')

    def test_enable_user_as_owner(self):
        self.disable(self.jared)
        self.login(self.jared)
        response = self.app.post('/users/jared/enable/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_allowed')
        self.assertEqual(user['message'], 'Permission denied.')

    @patch('ctf_api.models.session_manager.SessionManager.session.commit')
    def test_enable_user_with_unexpected_error(self, commit):
        self.disable(self.jared)
        self.login(self.admin)
        commit.side_effect = Exception('Unexpected')
        response = self.app.post('/users/jared/enable/')
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_working')
        self.assertEqual(user['message'], 'Something went wrong.')
