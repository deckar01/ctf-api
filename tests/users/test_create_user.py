from flask import json
from unittest.mock import patch
from tests.base_case import BaseCase
from ctf_api.config import Config
from ctf_api.models.user.user import User


class CreateUserCase(BaseCase):
    def setUp(self):
        super().setUp()
        self.invitation = self.create_user_invitation('gimme@test.com')
        self.bob_data = {
            'username': 'bob',
            'first_name': 'Bob',
            'last_name': 'Burr',
            'email_address': 'bob@test.com',
            'password': 'asspayorway',
        }

    def tearDown(self):
        super().tearDown()
        Config.REQUIRE_USER_INVITATION = True

    def test_create_user_as_admin(self):
        self.login(self.admin)
        response = self.app.post('/users/', data=self.bob_data)
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(
            set(user.keys()),
            set([
                'username', 'first_name', 'last_name', 'email_address',
                'actions', 'id', 'enabled', 'date_created', 'is_admin',
                'team', 'team_invitations',
            ])
        )
        self.assertEqual(user['username'], 'bob')
        self.assertEqual(user['first_name'], 'Bob')
        self.assertEqual(user['last_name'], 'Burr')
        self.assertEqual(user['email_address'], 'bob@test.com')
        self.assertEqual(user['id'], 4)
        self.assertEqual(user['enabled'], True)
        self.assertEqual(user['is_admin'], False)
        self.assertEqual(user['team'], None)
        self.assertEqual(user['actions'], {
            'disable!': '/users/bob/disable/',
        })
        self.assertEqual(user['team_invitations'], [])

    def test_create_user_with_missing_args(self):
        self.login(self.admin)
        response = self.app.post('/users/', data={'username': 'bob'})
        self.assertEqual(response.status, '400 BAD REQUEST')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_valid')
        self.assertEqual(user['message'], {
            'email_address': ['Missing data for required field.'],
            'first_name': ['Missing data for required field.'],
            'last_name': ['Missing data for required field.'],
            'password': ['Missing data for required field.'],
        })

    def test_create_user_with_invitation(self):
        self.bob_data['invitation'] = self.invitation.token
        response = self.app.post('/users/', data=self.bob_data)
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(
            set(user.keys()),
            set([
                'username', 'first_name', 'last_name', 'email_address',
                'team', 'team_invitations',
            ])
        )
        self.assertEqual(user['username'], 'bob')
        self.assertEqual(user['first_name'], 'Bob')
        self.assertEqual(user['last_name'], 'Burr')
        self.assertEqual(user['email_address'], 'bob@test.com')
        self.assertEqual(user['team'], None)
        self.assertEqual(user['team_invitations'], [])

    def test_create_user_with_missing_invitation(self):
        response = self.app.post('/users/', data=self.bob_data)
        self.assertEqual(response.status, '403 FORBIDDEN')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_allowed')
        self.assertEqual(user['message'], 'You need an invitation to register.')

    def test_create_user_with_fake_invitation(self):
        self.bob_data['invitation'] = 'IC4NH4Z4CC355?'
        response = self.app.post('/users/', data=self.bob_data)
        self.assertEqual(response.status, '404 NOT FOUND')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_found')
        self.assertEqual(user['message'], 'Invitation not found.')

    def test_create_user_with_used_invitation(self):
        self.invitation.used = True
        self.session.add(self.invitation)
        self.session.commit()
        self.bob_data['invitation'] = self.invitation.token
        response = self.app.post('/users/', data=self.bob_data)
        self.assertEqual(response.status, '404 NOT FOUND')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_found')
        self.assertEqual(user['message'], 'Invitation not found.')

    def test_create_user_without_invitation(self):
        Config.REQUIRE_USER_INVITATION = False
        response = self.app.post('/users/', data=self.bob_data)
        self.assertEqual(response.status, '200 OK')
        user = json.loads(response.data)
        self.assertEqual(
            set(user.keys()),
            set([
                'username', 'first_name', 'last_name', 'email_address',
                'team', 'team_invitations',
            ])
        )
        self.assertEqual(user['username'], 'bob')
        self.assertEqual(user['first_name'], 'Bob')
        self.assertEqual(user['last_name'], 'Burr')
        self.assertEqual(user['email_address'], 'bob@test.com')
        self.assertEqual(user['team'], None)
        self.assertEqual(user['team_invitations'], [])

    def test_create_user_with_duplicate_email_address(self):
        self.bob_data['invitation'] = self.invitation.token
        self.bob_data['email_address'] = 'alice@test.com'
        response = self.app.post('/users/', data=self.bob_data)
        self.assertEqual(response.status, '400 BAD REQUEST')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_valid')
        self.assertEqual(user['message'], 'That email address is already taken.')

    def test_create_user_with_duplicate_username(self):
        self.bob_data['invitation'] = self.invitation.token
        self.bob_data['username'] = 'alice'
        response = self.app.post('/users/', data=self.bob_data)
        self.assertEqual(response.status, '400 BAD REQUEST')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_valid')
        self.assertEqual(user['message'], 'That username is already taken.')

    @patch('ctf_api.models.session_manager.SessionManager.session.commit')
    def test_create_user_with_unexpected_error(self, commit):
        commit.side_effect = Exception('Unexpected')
        self.bob_data['invitation'] = self.invitation.token
        response = self.app.post('/users/', data=self.bob_data)
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')
        user = json.loads(response.data)
        self.assertEqual(set(user.keys()), set(['status', 'message']))
        self.assertEqual(user['status'], 'not_working')
        self.assertEqual(user['message'], 'Something went wrong.')
        self.assertEqual(self.invitation.used, False)
