import os
import unittest
import tempfile
import sqlalchemy as sa
from flask import json
from ctf_api.routes.app import app
from ctf_api.models.session_manager import SessionManager
from ctf_api.models.base import Base
from ctf_api.models.user.user import User
from ctf_api.models.user.admin_membership import AdminMembership
from ctf_api.models.user.invitation import UserInvitation
from ctf_api.models.team.team import Team
from ctf_api.models.team.voucher import TeamVoucher


class BaseCase(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()
        self.db_fd, self.db_file = tempfile.mkstemp()
        engine = sa.create_engine('sqlite:///' + self.db_file)
        Base.metadata.create_all(engine)
        SessionManager.set_engine(engine)
        self.session = SessionManager.session
        self.admin = self.create_user({
            'email_address': 'admin@test.com',
            'password': 'P455W0RD',
            'username': 'admin',
            'first_name': 'Max',
            'last_name': 'Powers',
        })
        self.create_admin_membership(self.admin)
        self.jared = self.create_user({
            'email_address': 'jared@test.com',
            'password': 'password',
            'username': 'jared',
            'first_name': 'Jared',
            'last_name': 'Deckard',
        })
        self.alice = self.create_user({
            'email_address': 'alice@test.com',
            'password': 'zapsword',
            'username': 'alice',
            'first_name': 'Alice',
            'last_name': 'Serry',
        })
        self.a_team = self.create_team({
            'name': 'A Team',
        })
        self.b_team = self.create_team({
            'name': 'B Team',
        })
        self.set_team(self.jared, self.a_team)

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(self.db_file)

    def create_user(self, data):
        user = User(data)
        self.session.add(user)
        self.session.commit()
        return user

    def enable(self, record):
        record.enabled = True
        self.session.add(record)
        self.session.commit()

    def disable(self, record):
        record.enabled = False
        self.session.add(record)
        self.session.commit()

    def create_admin_membership(self, user):
        self.session.add(AdminMembership(user))
        self.session.commit()
        assert user.is_admin()

    def create_team(self, data):
        team = Team(data)
        self.session.add(team)
        self.session.commit()
        return team

    def set_team(self, user, team):
        user.team_id = team.id
        self.session.add(user)
        self.session.commit()

    def create_user_invitation(self, email_address):
        invitation = UserInvitation({'email_address': email_address})
        self.session.add(invitation)
        self.session.commit()
        return invitation

    def create_team_voucher(self, email_address):
        voucher = TeamVoucher({'email_address': email_address})
        self.session.add(voucher)
        self.session.commit()
        return voucher

    def login(self, user):
        with self.app.session_transaction() as session:
            session['user_id'] = user.id

    def logout(self):
        with self.app.session_transaction() as session:
            session['user_id'] = None
