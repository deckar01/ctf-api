from unittest.mock import patch
from flask import json
from tests.base_case import BaseCase


class CreateUserInvitationCase(BaseCase):
    def test_create_user_invitation_as_admin(self):
        self.login(self.admin)
        response = self.app.post('/user-invitations/', data={'email_address': 'guest@test.com'})
        self.assertEqual(response.status, '200 OK')
        invitation = json.loads(response.data)
        keys = set(['email_address', 'token', 'used', 'date_created', 'id'])
        self.assertEqual(set(invitation.keys()), keys)
        self.assertEqual(invitation['email_address'], 'guest@test.com')
        self.assertEqual(invitation['used'], False)
        self.assertEqual(invitation['id'], 1)

    def test_create_user_invitation_with_duplicate_email_address(self):
        self.create_user_invitation('popular@test.com')
        self.login(self.admin)
        response = self.app.post('/user-invitations/', data={'email_address': 'popular@test.com'})
        self.assertEqual(response.status, '400 BAD REQUEST')
        invitation = json.loads(response.data)
        self.assertEqual(set(invitation.keys()), set(['status', 'message']))
        self.assertEqual(invitation['status'], 'not_valid')
        self.assertEqual(invitation['message'], 'That email address is already taken.')

    @patch('ctf_api.models.session_manager.SessionManager.session.commit')
    def test_create_with_unexpected_error(self, commit):
        self.login(self.admin)
        commit.side_effect = Exception('Unexpected')
        response = self.app.post('/user-invitations/', data={'email_address': 'bombshell@test.com'})
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')
        invitation = json.loads(response.data)
        self.assertEqual(set(invitation.keys()), set(['status', 'message']))
        self.assertEqual(invitation['status'], 'not_working')
        self.assertEqual(invitation['message'], 'Something went wrong.')

    def test_create_user_invitation_as_public(self):
        response = self.app.post('/user-invitations/', data={'email_address': 'sneaker@test.com'})
        self.assertEqual(response.status, '403 FORBIDDEN')
        invitation = json.loads(response.data)
        self.assertEqual(set(invitation.keys()), set(['status', 'message']))
        self.assertEqual(invitation['status'], 'not_allowed')
        self.assertEqual(invitation['message'], 'Permission denied.')

    def test_create_user_invitation_as_internal(self):
        self.login(self.jared)
        response = self.app.post('/user-invitations/', data={'email_address': 'molerat@test.com'})
        self.assertEqual(response.status, '403 FORBIDDEN')
        invitation = json.loads(response.data)
        self.assertEqual(set(invitation.keys()), set(['status', 'message']))
        self.assertEqual(invitation['status'], 'not_allowed')
        self.assertEqual(invitation['message'], 'Permission denied.')
