from unittest.mock import patch
from flask import json
from tests.base_case import BaseCase


class ViewAllUserInvitationsCase(BaseCase):
    def setUp(self):
        super().setUp()
        self.create_user_invitation('alice@test.com')
        self.create_user_invitation('bob@test.com')

    def test_view_all_user_invitations_as_admin(self):
        self.login(self.admin)
        response = self.app.get('/user-invitations/')
        self.assertEqual(response.status, '200 OK')
        invitations = json.loads(response.data)
        self.assertEqual(len(invitations), 2)
        keys = set(['email_address', 'used', 'date_created', 'actions'])
        self.assertEqual(set(invitations[0].keys()), keys)
        self.assertEqual(set(invitations[1].keys()), keys)
        self.assertEqual(invitations[0]['email_address'], 'alice@test.com')
        self.assertEqual(invitations[0]['used'], False)
        self.assertEqual(invitations[0]['actions'], {'view': '/user-invitations/1/'})
        self.assertEqual(invitations[1]['email_address'], 'bob@test.com')
        self.assertEqual(invitations[1]['used'], False)
        self.assertEqual(invitations[1]['actions'], {'view': '/user-invitations/2/'})

    def test_view_all_user_invitations_as_public(self):
        response = self.app.get('/user-invitations/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        invitations = json.loads(response.data)
        self.assertEqual(set(invitations.keys()), set(['status', 'message']))
        self.assertEqual(invitations['status'], 'not_allowed')
        self.assertEqual(invitations['message'], 'Permission denied.')

    def test_view_all_user_invitations_as_internal(self):
        self.login(self.jared)
        response = self.app.get('/user-invitations/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        invitations = json.loads(response.data)
        self.assertEqual(set(invitations.keys()), set(['status', 'message']))
        self.assertEqual(invitations['status'], 'not_allowed')
        self.assertEqual(invitations['message'], 'Permission denied.')
