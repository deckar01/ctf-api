from unittest.mock import patch
from flask import json
from tests.base_case import BaseCase


class ViewUserInvitationCase(BaseCase):
    def setUp(self):
        super().setUp()
        self.create_user_invitation('alice@test.com')
        self.create_user_invitation('bob@test.com')

    def test_view_user_invitation_as_admin(self):
        self.login(self.admin)
        response = self.app.get('/user-invitations/1/')
        self.assertEqual(response.status, '200 OK')
        invitation = json.loads(response.data)
        keys = set(['id', 'email_address', 'token', 'used', 'date_created'])
        self.assertEqual(set(invitation.keys()), keys)
        self.assertEqual(invitation['id'], 1)
        self.assertEqual(invitation['email_address'], 'alice@test.com')
        self.assertEqual(invitation['used'], False)

    def test_view_user_invitation_as_public(self):
        response = self.app.get('/user-invitations/1/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        invitation = json.loads(response.data)
        self.assertEqual(set(invitation.keys()), set(['status', 'message']))
        self.assertEqual(invitation['status'], 'not_allowed')
        self.assertEqual(invitation['message'], 'Permission denied.')

    def test_view_user_invitation_as_internal(self):
        self.login(self.jared)
        response = self.app.get('/user-invitations/1/')
        self.assertEqual(response.status, '403 FORBIDDEN')
        invitation = json.loads(response.data)
        self.assertEqual(set(invitation.keys()), set(['status', 'message']))
        self.assertEqual(invitation['status'], 'not_allowed')
        self.assertEqual(invitation['message'], 'Permission denied.')
