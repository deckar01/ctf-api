import unittest
import json
from ctf_api.routes.app import app


class UsersRoutesCase(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        app.testing = True
        self.app = app.test_client()

    def test_404_handler(self):
        response = self.app.get('/junk/')
        data = json.loads(response.data)
        self.assertEqual(response.status, '404 NOT FOUND')
        self.assertEqual(set(data.keys()), set(['status', 'message']))
        self.assertEqual(data['status'], 'not_found')
        self.assertEqual(data['message'], 'Not found.')

    def test_405_handler(self):
        response = self.app.post('/')
        data = json.loads(response.data)
        self.assertEqual(response.status, '404 NOT FOUND')
        self.assertEqual(set(data.keys()), set(['status', 'message']))
        self.assertEqual(data['status'], 'not_found')
        self.assertEqual(data['message'], 'Not found.')

    def test_view_routes(self):
        response = self.app.get('/')
        data = json.loads(response.data)
        self.assertEqual(response.status, '200 OK')
        assert 'actions' in data
        self.assertEqual(set(data['actions'].keys()), set([
            'view users?',
            'view teams?',
            'login!',
            'logout!',
        ]))
