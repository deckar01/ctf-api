class Errors(object):
    class NotAllowed(Exception):
        """The model does not have a filter for the viewer"""
        pass

    class NotFound(Exception):
        """The database does not have the requested records"""
        pass

    class NotValid(Exception):
        """The data failed validation"""
        pass

    class NotWorking(Exception):
        """An operation failed unexpectedly"""
        pass
