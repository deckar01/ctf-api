class Blindfold(object):
    get_viewer = None

    @staticmethod
    def register_viewer(get_viewer):
        Blindfold.get_viewer = get_viewer
