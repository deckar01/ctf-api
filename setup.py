import sqlalchemy as sa
from ctf_api.models.base import Base
from ctf_api.models.user.user import User, AdminMembership
from ctf_api.routes.app import app
from ctf_api.models.session_manager import SessionManager

engine = sa.create_engine('sqlite:///test.db')
Base.metadata.create_all(engine)
SessionManager.set_engine(engine)
session = SessionManager.session

try:
    admin = User({
        'email_address': 'test@test.com',
        'password': 'password',
        'username': 'jared',
        'first_name': 'Jared',
        'last_name': 'Deckard',
    })
    session.add(admin)
    session.add(AdminMembership(admin))
    session.commit()
except sa.exc.IntegrityError:
    print('Admin already exists')
    session.rollback()
session.close()

app.run(threaded=True, host="127.0.0.1", port=4000)
