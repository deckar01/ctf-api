import sqlalchemy as sa
from slugify import slugify
from passlib.hash import argon2
from blindfold import Errors
from ctf_api.config import Config
from ctf_api.filters.user.user import UserFilter
from ctf_api.models.base import Base
from ctf_api.models.enableable import Enableable
from ctf_api.models.session_manager import SessionManager
from ctf_api.models.user.admin_membership import AdminMembership
from ctf_api.models.user.invitation import UserInvitation
from ctf_api.models.team.team import Team
from ctf_api.models.team.invitation import TeamInvitation


class User(Base, UserFilter, Enableable):
    __tablename__ = 'user'

    email_address = sa.Column(sa.String, unique=True)
    password_hash = sa.Column(sa.String)
    username = sa.Column(sa.String)
    username_slug = sa.Column(sa.String, unique=True)
    first_name = sa.Column(sa.String)
    last_name = sa.Column(sa.String)
    team_id = sa.Column(sa.Integer, sa.ForeignKey('team.id'))

    admin_membership = sa.orm.relationship('AdminMembership', back_populates='user')
    team = sa.orm.relationship('Team', back_populates='members')
    team_invitations = sa.orm.relationship('TeamInvitation', back_populates='user')

    def __init__(self, data):
        self.email_address = data['email_address']
        self.password_hash = argon2.hash(data['password'])
        self.username = data['username']
        self.username_slug = slugify(data['username'])
        self.first_name = data['first_name']
        self.last_name = data['last_name']

    def is_admin(self):
        return bool(self.admin_membership)

    @staticmethod
    def create(data, creator):
        if Config.REQUIRE_USER_INVITATION and not (creator and creator.is_admin()):
            if 'invitation' not in data:
                raise Errors.NotAllowed('You need an invitation to register.')
            invitation = UserInvitation.find(token=data['invitation'])
            if not invitation:
                raise Errors.NotFound('Invitation not found.')
            invitation.used = True
            SessionManager.session.add(invitation)

        user = User(data)
        try:
            SessionManager.session.add(user)
            SessionManager.session.commit()
            return user
        except sa.exc.IntegrityError:
            SessionManager.session.rollback()
            User.check_for_existing('email_address', user.email_address)
            User.check_for_existing('username_slug', user.username_slug, description='username')
        except Exception:
            SessionManager.session.rollback()
        raise Errors.NotWorking('Something went wrong.')

    @staticmethod
    def get_by_login(credentials):
        username_slug = slugify(credentials['username'])
        password = credentials['password']
        user = User.get_by_username(username_slug)
        if argon2.verify(password, user.password_hash):
            return user
        else:
            raise Errors.NotAllowed('Incorrect password.')

    @staticmethod
    def get_by_username(username_slug):
        query = SessionManager.session.query(User)
        query = query.filter(User.username_slug == username_slug)
        record = query.first()
        if not record:
            raise Errors.NotFound('User not found.')
        return record
