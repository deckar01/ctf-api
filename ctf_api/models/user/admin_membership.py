import sqlalchemy as sa
from ctf_api.models.base import Base


class AdminMembership(Base):
    __tablename__ = 'admin_membership'

    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))

    user = sa.orm.relationship('User', back_populates='admin_membership')

    def __init__(self, user):
        self.user = user
