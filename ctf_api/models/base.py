from datetime import datetime
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from blindfold import Errors
from ctf_api.models.session_manager import SessionManager


class BaseExtensions(object):
    id = sa.Column(sa.Integer, primary_key=True)
    date_created = sa.Column(sa.DateTime, default=datetime.utcnow)

    @classmethod
    def get_by_id(Model, id):
        query = SessionManager.session.query(Model).filter(Model.id == id)
        record = query.first()
        return record

    @classmethod
    def get_all(Model):
        query = SessionManager.session.query(Model)
        records = query.all()
        return records

    @classmethod
    def check_for_existing(Model, column_name, value, description=None):
        column = getattr(Model, column_name)
        query = SessionManager.session.query(Model)
        query = query.filter(column == value)
        user = query.first()
        if user:
            column_description = description or column_name.replace('_', ' ')
            message = 'That {} is already taken.'.format(column_description)
            raise Errors.NotValid(message)


Base = declarative_base(cls=BaseExtensions)
