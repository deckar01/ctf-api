from sqlalchemy.orm import scoped_session, sessionmaker


class SessionManager(object):
    @staticmethod
    def set_engine(engine):
        SessionManager.engine = engine
        SessionManager.session = scoped_session(sessionmaker(bind=engine))


SessionManager.engine = None
SessionManager.session = None
