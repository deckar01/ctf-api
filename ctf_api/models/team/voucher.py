import sqlalchemy as sa
from marshmallow import fields
from flask_marshmallow import fields as flask_fields
import secrets
from blindfold import Errors
from ctf_api.models.base import Base
from ctf_api.filters.team.voucher import TeamVoucherFilter
from ctf_api.models.session_manager import SessionManager


class TeamVoucher(Base, TeamVoucherFilter):
    __tablename__ = 'team_voucher'

    email_address = sa.Column(sa.String, unique=True)
    token = sa.Column(sa.String)
    used = sa.Column(sa.Boolean, default=False)

    def __init__(self, data):
        self.email_address = data['email_address']
        self.token = secrets.token_urlsafe(16)

    @staticmethod
    def create(data, creator):
        if creator and creator.is_admin():
            voucher = TeamVoucher(data)
            try:
                SessionManager.session.add(voucher)
                SessionManager.session.commit()
                return voucher
            except sa.exc.IntegrityError as e:
                SessionManager.session.rollback()
                TeamVoucher.check_for_existing('email_address', voucher.email_address)
            except Exception:
                SessionManager.session.rollback()
            raise Errors.NotWorking('Something went wrong.')
        else:
            raise Errors.NotAllowed('Permission denied.')

    @staticmethod
    def find(token):
        query = SessionManager.session.query(TeamVoucher)
        query = query.filter(TeamVoucher.token == token)
        query = query.filter(TeamVoucher.used.is_(False))
        record = query.first()
        return record
