import sqlalchemy as sa
from slugify import slugify
from blindfold import Errors
from ctf_api.config import Config
from ctf_api.filters.team.team import TeamFilter
from ctf_api.models.base import Base
from ctf_api.models.enableable import Enableable
from ctf_api.models.session_manager import SessionManager
from ctf_api.models.team.voucher import TeamVoucher


class Team(Base, TeamFilter, Enableable):
    __tablename__ = 'team'

    name = sa.Column(sa.String)
    name_slug = sa.Column(sa.String, unique=True)

    members = sa.orm.relationship('User', back_populates='team')
    invitations = sa.orm.relationship('TeamInvitation', back_populates='team')

    def __init__(self, data):
        self.name = data['name']
        self.name_slug = slugify(data['name'])

    @staticmethod
    def create(data, creator):
        if not creator:
            raise Errors.NotAllowed('You must be logged in to create a team.')
        if Config.REQUIRE_TEAM_VOUCHER and not creator.is_admin():
            if 'voucher' not in data:
                raise Errors.NotAllowed('You need a voucher to create a team.')
            voucher = TeamVoucher.find(token=data['voucher'])
            if not voucher:
                raise Errors.NotFound('Voucher not found.')
            voucher.used = True
            SessionManager.session.add(voucher)

        team = Team(data)

        if not creator.is_admin():
            creator.team = team
            SessionManager.session.add(creator)

        try:
            SessionManager.session.add(team)
            SessionManager.session.commit()
            return team
        except sa.exc.IntegrityError as e:
            SessionManager.session.rollback()
            Team.check_for_existing('name_slug', team.name_slug, description='team name')
        except Exception:
            SessionManager.session.rollback()
        raise Errors.NotWorking('Something went wrong.')

    @staticmethod
    def get_by_name(name_slug):
        query = SessionManager.session.query(Team)
        query = query.filter(Team.name_slug == name_slug)
        record = query.first()
        if not record:
            raise Errors.NotFound('Team not found.')
        return record

    @staticmethod
    def join(invitation, joiner):
        if not joiner:
            raise Errors.NotAllowed('You must be logged in to join a team.')
        team_invitation = TeamInvitation.find(token=invitation)
        if not team_invitation:
            raise Errors.NotFound('Team invitation not found.')
        team_invitation.used = True
        SessionManager.session.add(team_invitation)
        user.team = team_invitation.team
        SessionManager.session.add(user)
        SessionManager.session.commit()
