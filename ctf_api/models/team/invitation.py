import sqlalchemy as sa
from marshmallow import fields
from flask_marshmallow import fields as flask_fields
import secrets
from blindfold import Errors
from ctf_api.models.base import Base
from ctf_api.filters.team.invitation import TeamInvitationFilter
from ctf_api.models.session_manager import SessionManager


class TeamInvitation(Base, TeamInvitationFilter):
    __tablename__ = 'team_invitation'

    team_id = sa.Column(sa.Integer, sa.ForeignKey('team.id'))
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    token = sa.Column(sa.String)
    used = sa.Column(sa.Boolean, default=False)

    team = sa.orm.relationship('Team', back_populates='invitations')
    user = sa.orm.relationship('User', back_populates='team_invitations')

    def __init__(self, data):
        self.email_address = data['email_address']
        self.token = secrets.token_urlsafe(16)

    @staticmethod
    def create(data, creator):
        if creator and creator.is_admin():
            invitation = TeamInvitation(data)
            try:
                SessionManager.session.add(invitation)
                SessionManager.session.commit()
                return invitation
            except sa.exc.IntegrityError as e:
                SessionManager.session.rollback()
                TeamInvitation.check_for_existing('email_address', invitation.email_address)
            except Exception:
                SessionManager.session.rollback()
            raise Errors.NotWorking('Something went wrong.')
        else:
            raise Errors.NotAllowed('Permission denied.')

    @staticmethod
    def find(token):
        query = SessionManager.session.query(TeamInvitation)
        query = query.filter(TeamInvitation.token == token)
        query = query.filter(TeamInvitation.used.is_(False))
        record = query.first()
        return record
