import sqlalchemy as sa
from blindfold import Errors
from ctf_api.models.session_manager import SessionManager


class Enableable(object):
    enabled = sa.Column(sa.Boolean, default=True)

    def set_enabled(self, value, editor):
        if not editor or not editor.is_admin():
            raise Errors.NotAllowed('Permission denied.')

        self.enabled = value
        try:
            SessionManager.session.add(self)
            SessionManager.session.commit()
            return self
        except Exception:
            SessionManager.session.rollback()
        raise Errors.NotWorking('Something went wrong.')
