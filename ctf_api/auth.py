from flask import session
from ctf_api.models.user.user import User


def current_user():
    user_id = session.get('user_id')
    return User.get_by_id(user_id)
