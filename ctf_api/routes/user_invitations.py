from flask import Blueprint
from webargs.flaskparser import use_args
from ctf_api.auth import current_user
from ctf_api.models.user.invitation import UserInvitation

user_invitations_blueprint = Blueprint('user_invitations', __name__)


@user_invitations_blueprint.route('/user-invitations/')
@UserInvitation.filter_response(many=True)
def view_all_user_invitations():
    return UserInvitation.get_all()


@user_invitations_blueprint.route('/user-invitations/<int:id>/')
@UserInvitation.filter_response()
def view_user_invitation(id):
    return UserInvitation.get_by_id(id)


@user_invitations_blueprint.route('/user-invitations/', methods=['POST'])
@use_args(UserInvitation.CreateFilter)
@UserInvitation.filter_response()
def create_user_invitation(invitation_data):
    return UserInvitation.create(invitation_data, creator=current_user())
