from flask import Blueprint
from webargs.flaskparser import use_args
from ctf_api.auth import current_user
from ctf_api.models.team.team import Team

teams_blueprint = Blueprint('teams', __name__)


@teams_blueprint.route('/teams/', methods=['POST'])
@use_args(Team.CreateFilter)
@Team.filter_response()
def create_team(team_data):
    return Team.create(team_data, creator=current_user())


@teams_blueprint.route('/teams/')
@Team.filter_response(many=True)
def view_all_teams():
    return Team.get_all()


@teams_blueprint.route('/teams/<string:name_slug>/')
@Team.filter_response()
def view_team(name_slug):
    return Team.get_by_name(name_slug)


@teams_blueprint.route('/join/<string:invitation>/', methods=['POST'])
@Team.filter_response()
def join_team(invitation):
    return Team.join(invitation, user=current_user())


@teams_blueprint.route('/teams/<string:name_slug>/enable/', methods=['POST'])
@Team.filter_response()
def enable_team(name_slug):
    team = Team.get_by_name(name_slug)
    team.set_enabled(True, editor=current_user())
    return team


@teams_blueprint.route('/teams/<string:name_slug>/disable/', methods=['POST'])
@Team.filter_response()
def disable_team(name_slug):
    team = Team.get_by_name(name_slug)
    team.set_enabled(False, editor=current_user())
    return team
