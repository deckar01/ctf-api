from flask import Flask, jsonify
from flask_cors import CORS
from webargs.flaskparser import parser
from blindfold import Blindfold, Errors
from ctf_api.auth import current_user
from ctf_api.routes.auth import auth_blueprint
from ctf_api.routes.users import users_blueprint
from ctf_api.routes.user_invitations import user_invitations_blueprint
from ctf_api.routes.teams import teams_blueprint

app = Flask(__name__)
app.config.from_object('ctf_api.config.Config')
CORS(app, supports_credentials=True)

Blindfold.register_viewer(current_user)


@app.route('/')
def view_routes():
    return jsonify({
        'actions': {
            'view users?': '/users/',
            'view teams?': '/teams/',
            'login!': '/login/ username password',
            'logout!': '/logout/',
        }
    })


app.register_blueprint(auth_blueprint)
app.register_blueprint(users_blueprint)
app.register_blueprint(user_invitations_blueprint)
app.register_blueprint(teams_blueprint)


def error_to_json(error, status):
    return jsonify({'status': status, 'message': error.args[0]})


@app.errorhandler(Errors.NotValid)
def handle_not_valid(error):
    return error_to_json(error, 'not_valid'), 400


@app.errorhandler(Errors.NotAllowed)
def handle_not_allowed(error):
    return error_to_json(error, 'not_allowed'), 403


@app.errorhandler(Errors.NotFound)
def handle_not_found(error):
    return error_to_json(error, 'not_found'), 404


@app.errorhandler(Errors.NotWorking)
def handle_not_working(error):
    return error_to_json(error, 'not_working'), 500


@app.errorhandler(404)
def handle_404(error):
    return handle_not_found(Exception('Not found.'))


@app.errorhandler(405)
def handle_405(error):
    return handle_not_found(Exception('Not found.'))


@parser.error_handler
def handle_error(error):
    raise Errors.NotValid(error.args[0])
