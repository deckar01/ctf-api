from flask import Blueprint, jsonify, session
from webargs.flaskparser import use_args
from ctf_api.models.user.user import User

auth_blueprint = Blueprint('auth', __name__)


@auth_blueprint.route('/login/', methods=['POST'])
@use_args(User.LoginFilter)
def login(credentials):
    user = User.get_by_login(credentials)
    session['user_id'] = user.id
    return jsonify({'status': 'success', 'message': 'You are logged in.'})


@auth_blueprint.route('/logout/', methods=['POST'])
def logout():
    session['user_id'] = None
    return jsonify({'status': 'success', 'message': 'You are logged out.'})
