from flask import Blueprint
from webargs.flaskparser import use_args
from ctf_api.auth import current_user, session
from ctf_api.models.user.user import User

users_blueprint = Blueprint('users', __name__)


@users_blueprint.route('/users/', methods=['POST'])
@use_args(User.CreateFilter)
@User.filter_response()
def create_user(user_data):
    user = User.create(user_data, creator=current_user())
    # Log new users in
    if not current_user():
        session['user_id'] = user.id
    return user


@users_blueprint.route('/users/')
@User.filter_response(many=True)
def view_all_users():
    return User.get_all()


@users_blueprint.route('/users/<string:username_slug>/')
@User.filter_response()
def view_user(username_slug):
    user = User.get_by_username(username_slug)
    return user


@users_blueprint.route('/users/<string:username_slug>/enable/', methods=['POST'])
@User.filter_response()
def enable_user(username_slug):
    user = User.get_by_username(username_slug)
    user.set_enabled(True, editor=current_user())
    return user


@users_blueprint.route('/users/<string:username_slug>/disable/', methods=['POST'])
@User.filter_response()
def disable_user(username_slug):
    user = User.get_by_username(username_slug)
    user.set_enabled(False, editor=current_user())
    return user
