from flask import Blueprint
from webargs.flaskparser import use_args
from ctf_api.auth import current_user
from ctf_api.models.team.voucher import TeamVoucher

team_vouchers_blueprint = Blueprint('team_vouchers', __name__)


@team_vouchers_blueprint.route('/team-vouchers/')
@TeamVoucher.filter_response(many=True)
def view_all_team_vouchers():
    return TeamVoucher.get_all()


@team_vouchers_blueprint.route('/team-vouchers/<int:id>/')
@TeamVoucher.filter_response()
def view_team_voucher(id):
    return TeamVoucher.get_by_id(id)


@team_vouchers_blueprint.route('/team-vouchers/', methods=['POST'])
@use_args(TeamVoucher.CreateFilter)
@TeamVoucher.filter_response()
def create_team_voucher(voucher_data):
    return TeamVoucher.create(voucher_data, creator=current_user())
