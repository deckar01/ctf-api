class EnableableFilter(object):
    @staticmethod
    def filter_actions(data, model):
        if model.enabled:
            del data['actions']['enable!']
        else:
            del data['actions']['disable!']
