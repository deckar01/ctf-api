from marshmallow import fields
from flask_marshmallow import fields as flask_fields
from blindfold import ViewFilter, ViewFilterMixin
from ctf_api.filters.arg_filter import ArgFilter
from ctf_api.filters.user.user_team import UserTeamViewFilter


class TeamInvitationViewFilter(ViewFilter):
    team = fields.Nested(UserTeamViewFilter)
    date_created = fields.DateTime()

    actions = flask_fields.Hyperlinks({
        'join!': flask_fields.URLFor('team.join', invitation='<token>'),
    })

    @staticmethod
    def visibility(record):
        return True
