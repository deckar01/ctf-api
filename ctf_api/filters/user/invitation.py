from marshmallow import fields
from flask_marshmallow import fields as flask_fields
from blindfold import ViewFilter, ViewFilterMixin
from ctf_api.filters.arg_filter import ArgFilter


class UserInvitationFilter(ViewFilterMixin):
    class CreateFilter(ArgFilter):
        email_address = fields.Email(required=True)

    class AdminViewFilter(ViewFilter):
        id = fields.Int()
        email_address = fields.Str()
        token = fields.Str()
        used = fields.Bool()
        date_created = fields.DateTime()

        @staticmethod
        def visibility(record):
            return True

    class AdminListViewFilter(ViewFilter):
        email_address = fields.Str()
        used = fields.Bool()
        date_created = fields.DateTime()

        actions = flask_fields.Hyperlinks({
            'view': flask_fields.URLFor('user_invitations.view_user_invitation', id='<id>'),
        })

        @staticmethod
        def visibility(record):
            return True
