from marshmallow import fields
from flask_marshmallow import fields as flask_fields
from blindfold import ViewFilter


class UserTeamViewFilter(ViewFilter):
    name = fields.Str()

    actions = flask_fields.Hyperlinks({
        'view?': flask_fields.URLFor('teams.view_team', name_slug='<name_slug>'),
    })

    @staticmethod
    def visibility(team):
        return team.enabled
