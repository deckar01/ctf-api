from marshmallow import fields, post_dump
from flask_marshmallow import fields as flask_fields
from blindfold import ViewFilter, ViewFilterMixin
from ctf_api.filters.arg_filter import ArgFilter
from ctf_api.filters.enableable_filter import EnableableFilter
from ctf_api.filters.user.user_team import UserTeamViewFilter
from ctf_api.filters.user.team_invitation import TeamInvitationViewFilter


class UserFilter(ViewFilterMixin):
    def has_owner(self, viewer):
        return self.id == viewer.id

    class CreateFilter(ArgFilter):
        email_address = fields.Email(required=True)
        password = fields.Str(required=True)
        username = fields.Str(required=True)
        first_name = fields.Str(required=True)
        last_name = fields.Str(required=True)
        invitation = fields.Str()

    class LoginFilter(ArgFilter):
        username = fields.Str(required=True)
        password = fields.Str(required=True)

    class PublicViewFilter(ViewFilter):
        username = fields.Str()
        first_name = fields.Str()
        last_name = fields.Str()
        team = fields.Nested(UserTeamViewFilter)

        @staticmethod
        def visibility(user):
            return user.enabled

    class InternalViewFilter(PublicViewFilter):
        pass

    class OwnerViewFilter(InternalViewFilter):
        email_address = fields.Email()
        team_invitations = fields.Nested(TeamInvitationViewFilter, many=True)

    class AdminViewFilter(OwnerViewFilter):
        id = fields.Int()
        enabled = fields.Bool()
        is_admin = fields.Bool()
        date_created = fields.DateTime()

        actions = flask_fields.Hyperlinks({
            'enable!': flask_fields.URLFor('users.enable_user', username_slug='<username_slug>'),
            'disable!': flask_fields.URLFor('users.disable_user', username_slug='<username_slug>'),
        })

        @post_dump(pass_original=True)
        def filter_links(self, data, user):
            EnableableFilter.filter_actions(data, user)

        @staticmethod
        def visibility(user):
            return True

    class PublicListViewFilter(PublicViewFilter):
        actions = flask_fields.Hyperlinks({
            'view?': flask_fields.URLFor('users.view_user', username_slug='<username_slug>'),
        })

    class InternalListViewFilter(PublicListViewFilter):
        pass

    class AdminListViewFilter(InternalListViewFilter):
        id = fields.Int()
        enabled = fields.Bool()
        is_admin = fields.Bool()
        date_created = fields.DateTime()
        email_address = fields.Email()

        actions = flask_fields.Hyperlinks({
            'view?': flask_fields.URLFor('users.view_user', username_slug='<username_slug>'),
            'enable!': flask_fields.URLFor('users.enable_user', username_slug='<username_slug>'),
            'disable!': flask_fields.URLFor('users.disable_user', username_slug='<username_slug>'),
        })

        @post_dump(pass_original=True)
        def filter_links(self, data, user):
            EnableableFilter.filter_actions(data, user)

        @staticmethod
        def visibility(user):
            return True
