from marshmallow import fields, post_dump
from flask_marshmallow import fields as flask_fields
from blindfold import ViewFilter, ViewFilterMixin
from ctf_api.filters.arg_filter import ArgFilter
from ctf_api.filters.enableable_filter import EnableableFilter
from ctf_api.filters.team.team_member import TeamMemberListViewFilter, TeamMemberAdminListViewFilter
from ctf_api.filters.team.team_invitation import TeamInvitationViewFilter


class TeamFilter(ViewFilterMixin):
    def has_member(self, viewer):
        return viewer.team_id == self.id

    class CreateFilter(ArgFilter):
        name = fields.Str(required=True)
        voucher = fields.Str()

    class PublicViewFilter(ViewFilter):
        name = fields.Str()
        members = fields.Nested(TeamMemberListViewFilter, many=True)

        @staticmethod
        def visibility(team):
            return team.enabled

    class InternalViewFilter(PublicViewFilter):
        pass

    class MemberViewFilter(InternalViewFilter):
        invitations = fields.Nested(TeamInvitationViewFilter, many=True)

    class AdminViewFilter(MemberViewFilter):
        id = fields.Int()
        enabled = fields.Bool()
        date_created = fields.DateTime()
        members = fields.Nested(TeamMemberAdminListViewFilter, many=True)

        actions = flask_fields.Hyperlinks({
            'enable!': flask_fields.URLFor('teams.enable_team', name_slug='<name_slug>'),
            'disable!': flask_fields.URLFor('teams.disable_team', name_slug='<name_slug>'),
        })

        @post_dump(pass_original=True)
        def filter_links(self, data, team):
            EnableableFilter.filter_actions(data, team)

        @staticmethod
        def visibility(user):
            return True

    class PublicListViewFilter(ViewFilter):
        name = fields.Str()

        actions = flask_fields.Hyperlinks({
            'view?': flask_fields.URLFor('teams.view_team', name_slug='<name_slug>'),
        })

        @staticmethod
        def visibility(team):
            return team.enabled

    class InternalListViewFilter(PublicListViewFilter):
        pass

    class AdminListViewFilter(InternalListViewFilter):
        id = fields.Int()
        enabled = fields.Bool()
        date_created = fields.DateTime()

        actions = flask_fields.Hyperlinks({
            'view?': flask_fields.URLFor('teams.view_team', name_slug='<name_slug>'),
            'enable!': flask_fields.URLFor('teams.enable_team', name_slug='<name_slug>'),
            'disable!': flask_fields.URLFor('teams.disable_team', name_slug='<name_slug>'),
        })

        @post_dump(pass_original=True)
        def filter_links(self, data, team):
            EnableableFilter.filter_actions(data, team)

        @staticmethod
        def visibility(user):
            return True
