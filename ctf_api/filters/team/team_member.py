from marshmallow import fields
from flask_marshmallow import fields as flask_fields
from blindfold import ViewFilter


class TeamMemberListViewFilter(ViewFilter):
    username = fields.Str()

    actions = flask_fields.Hyperlinks({
        'view?': flask_fields.URLFor('users.view_user', username_slug='<username_slug>'),
    })

    @staticmethod
    def visibility(user):
        return user.enabled


class TeamMemberAdminListViewFilter(TeamMemberListViewFilter):
    @staticmethod
    def visibility(user):
        return True
