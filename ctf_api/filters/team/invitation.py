from marshmallow import fields
from flask_marshmallow import fields as flask_fields
from blindfold import ViewFilter, ViewFilterMixin
from ctf_api.filters.arg_filter import ArgFilter
from ctf_api.filters.team.team_member import TeamMemberAdminListViewFilter


class TeamInvitationFilter(ViewFilterMixin):
    class CreateFilter(ArgFilter):
        username = fields.Str(required=True)

    class AdminViewFilter(ViewFilter):
        id = fields.Int()
        user = fields.Nested(TeamMemberAdminListViewFilter)
        token = fields.Str()
        used = fields.Bool()
        date_created = fields.DateTime()

        @staticmethod
        def visibility(record):
            return True

    class AdminListViewFilter(ViewFilter):
        user = fields.Nested(TeamMemberAdminListViewFilter)
        used = fields.Bool()
        date_created = fields.DateTime()

        actions = flask_fields.Hyperlinks({
            'view': flask_fields.URLFor('team_invitations.view_team_invitation', id='<id>'),
        })

        @staticmethod
        def visibility(record):
            return True
