from marshmallow import Schema


class ArgFilter(Schema):
    class Meta:
        strict = True
