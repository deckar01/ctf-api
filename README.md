# CTF API ![ctf-api_icon](/uploads/492795921e224e2e169961782f6f70b7/ctf-api_icon.png)

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![pipeline status](https://gitlab.com/deckar01/ctf-api/badges/master/pipeline.svg)](https://gitlab.com/deckar01/ctf-api/commits/master) [![coverage report](https://gitlab.com/deckar01/ctf-api/badges/master/coverage.svg)](https://gitlab.com/deckar01/ctf-api/commits/master)

**Status**: Work in progress

### Completed Features

- Users
- Admin Membership
- User Invitations
- Teams
- Team Member Invitations
- Team Vouchers

### Planned Features

- Email Verification
- Password Reset
- Flags
- Captures

## Setup

```
# Python dependencies
virtualenv -p python3 .env
source .env/bin/activate
pip install -r requirements.txt

# Run the API
python setup.py &

# Serve the terminal client
cd ctf_client
python -m http.server 8000 &

# Explore the API at http://localhost:8000/
```
