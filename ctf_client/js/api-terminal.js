class APITerminal {
  constructor(el, server) {
    this.el = el;
    this.server = server;
    this.currentDirectory = '/';

    try { this.history = JSON.parse(localStorage.history || '[]'); }
    catch(e) { this.history = []; }
    this.historyCursor = -1;

    this.commands = [];
    this.registerCommand('help', this.help, {
      description: 'Get help with commands.',
      usage: 'help [command]',
      args: {
        command: {
          optional: true,
          description: 'The command to get help with.',
          examples: ['cd', 'get']
        }
      }
    });
    this.registerCommand('cd', this.cd, {
      description: 'Change the current directory or resource.',
      usage: 'cd [path]',
      args: {
        path: {
          optional: true,
          description: 'The path to the directory or resource.',
          examples: ['test', './test', '../test', '/test', 't/e/s/t']
        }
      }
    });
    this.registerCommand('get', this.get, {
      description: 'View a directory or resource.',
      usage: 'get [path]',
      aliases: ['ls'],
      args: {
        path: {
          optional: true,
          description: 'The path to the directory or resource.',
          examples: ['test', './test', '../test', '/test', 't/e/s/t']
        }
      }
    });
    this.registerCommand('post', this.post, {
      description: 'Send data to the remote server.',
      usage: '[post] path [data ...]',
      args: {
        path: {
          optional: true,
          description: 'The path to the directory or resource to view',
          examples: ['test', './test', '../test', '/test', 't/e/s/t']
        },
        data: {
          optional: true,
          many: true,
          description: 'The key=value pairs to send to the remote server. If the value is omitted, a masked prompt will ask for the value so that it can be entered securely.',
          examples: ['name=jared', 'food=bacon', 'password']
        }
      }
    });
    this.registerCommand('clear', this.clear, {
      description: 'Clear the terminal.',
      usage: 'clear'
    });

    this.runningContext = this.newContext();
  }

  async newContext() {
    const prefix = `${this.server}${this.currentDirectory} `;
    this.context = new APITerminalContext(this, prefix);
    this.context.prompt.input.on('up', () => {
      if(this.historyCursor + 1 < this.history.length) {
        this.historyCursor += 1;
        this.context.prompt.input.set(this.history[this.historyCursor]);
      }
    });
    this.context.prompt.input.on('down', () => {
      if(this.historyCursor >= 0) {
        this.historyCursor -= 1;
        this.context.prompt.input.set(this.history[this.historyCursor] || '');
      }
    });
    this.context.prompt.input.on('keypress', () => this.scrollToBottom());
    this.el.appendChild(this.context.el);
    this.context.prompt.input.focus();

    try {
      const command = await this.context.prompt.input.value;
      if(command) {
        this.saveCommand(command);
        const [cmd, args] = this.parseArgs(command);
        await this.runCommand(cmd, args);
      }
      this.runningContext = this.newContext();
    } catch(e) {
      console.error(e);
    }
  }

  scrollToBottom() {
    window.scrollTo(0, this.el.scrollHeight);
  }

  resolvePath(path) {
    const parts = path.match(/^\/|[^\/]+/g);
    let stack = [];
    if(parts[0] === '/') {
      parts.shift();
    } else {
      stack = this.currentDirectory.match(/[^\/]+/g) || [];
    }
    for(const part of parts) {
      if(!part || part == '.') continue;
      if(part == '..') {
        if(stack.length > 0) stack.pop();
        continue;
      }
      stack.push(part);
    }
    if(stack.length) return `/${stack.join('/')}/`;
    else return '/';
  }

  async fetch(...args) {
    const response = await fetch(...args);
    const data = await response.json();
    if(response.ok) return data;
    else throw data;
  }

  async get(context, path='.') {
    const options = {credentials: 'include'};
    const data = await this.fetch(`${this.server}${this.resolvePath(path)}`, options);
    if(context) context.log(data, 'ok');
    return data;
  }

  parseArgs(command) {
    const parts = command.match(/"(\\"|[^"])*"|(\\\s|\S)+/g)
    .map((part) => part[0] == '"' ? part.substr(1, part.length - 2) : part)
    .map((part) => part.replace(/\\\s/g, ' '))
    .map((part) => part.replace(/\\"/g, '"'));
    return [parts[0], parts.slice(1)];
  }

  saveCommand(command) {
    if(command) {
      this.history.unshift(command);
      localStorage.history = JSON.stringify(this.history);
    }
    this.historyCursor = -1;
  }

  registerCommand(cmd, handler, help) {
    this.commands[cmd] = handler;
    this.commands[cmd].help = help;
    if(help.aliases) {
      for(const alias of help.aliases) {
        this.commands[alias] = handler;
        this.commands[alias].help = help;
      }
    }
  }

  async runCommand(cmd, args) {
    try {
      if(this.commands[cmd]) {
        await this.commands[cmd].call(this, this.context, ...args);
      } else {
        await this.post(this.context, cmd, ...args);
      }
    } catch(error) {
      this.context.log(error, 'error');
    }
  }

  run(command) {
    this.context.prompt.input.set(command);
    this.context.prompt.input.submit(command);
    this.context.prompt.input.teardown();
  }

  help(context, command) {
    if(command) {
      if(this.commands[command]) {
        context.log(this.commands[command].help, 'ok');
      } else {
        context.log('Command not found.', 'error');
      }
    } else {
      const output = {};
      for(const command in this.commands) {
        const help = this.commands[command].help;
        output[help.usage] = help.description;
      }
      context.log(output, 'ok');
    }
  }

  clear(context) {
    this.el.innerHTML = '';
  }

  async cd(context, path) {
    if(path) {
      await this.get(null, path);
      this.currentDirectory = this.resolvePath(path);
      context.setStatus('success');
    }
  }

  async post(context, path, ...args) {
    path = this.resolvePath(path);
    if(path.indexOf('=') >= 0) {
      args.unshift(path);
    }
    const data = {};
    for(const arg of args) {
      const [key, ...value] = arg.split(/=/);
      if(value.length === 0) {
        const namespace = `${path}?${key}`;
        const message = `Enter the ${key}: `;
        if(key === 'password') {
          data[key] = await context.promptSecret(message, namespace);
        } else {
          data[key] = await context.promptInput(message);
        }
      } else {
        data[key] = value.join('=');
      }
    }
    const options = {
      method: 'POST',
      headers: new Headers({'Content-Type': 'application/json'}),
      body: JSON.stringify(data),
      credentials: 'include',
    };
    const response = await this.fetch(`${this.server}${path}`, options);
    context.log(response, 'success');
  }
}

class APITerminalContext {
  constructor(terminal, prefix='') {
    this.terminal = terminal;
    this.el = document.createElement('div');
    this.el.classList.add('terminal-context');
    this.prompt = new APITerminalCommandPrompt(prefix);
    this.el.appendChild(this.prompt.el);
    this.status = document.createElement('div');
    this.status.classList.add('terminal-context-status');
    this.prompt.el.appendChild(this.status);
    this.collapsable = false;
  }

  log(data, status) {
    const el = document.createElement('div');
    this.setStatus(status);
    el.classList.add(status);
    if(!this.collapsable) {
      this.collapsable = true;
      this.el.classList.add('collapsable');
      this.prompt.el.addEventListener('click', (e) => {
        e.preventDefault();
        this.el.classList.toggle('hide');
      });
    }
    el.appendChild(this.format(data));
    this.el.appendChild(el);
  }

  setStatus(status) {
    this.status.classList.add(status);
    const lastContext = this.el.previousElementSibling;
    if(lastContext && lastContext.classList.contains('collapsable')) {
      lastContext.classList.add('hide');
    }
  }

  format(object, key) {
    if(typeof object === 'string') {
      if(key && key.slice(-1) === '?') {
        return this.formatLink(object, 'GET');
      }
      if(key && key.slice(-1) === '!') {
        return this.formatLink(object, 'POST');
      }
      return this.formatScalar('string', object);
    }
    if(typeof object === 'number') {
      return this.formatScalar('number', object);
    }
    if(typeof object === 'boolean') {
      return this.formatScalar('boolean', object);
    }
    if(!object) {
      return this.formatScalar('null', 'none');
    }
    if(object instanceof Array) {
      return this.formatArray(object);
    }
    return this.formatObject(object);
  }

  formatScalar(type, text) {
    const el = document.createElement('span');
    el.classList.add(type);
    el.innerText = text;
    return el;
  }

  formatLink(url, method) {
    const commands = {
      GET: 'get',
      POST: 'post',
    };
    const command = `${commands[method]} ${url}`;
    const el = document.createElement('a');
    const urlParts = url.split(' ');
    const path = urlParts[0];
    const params = urlParts.slice(1);
    const encodedURL = path + (params.length ? '?' + params.join('&') : '')
    el.href = `#${method}:${encodedURL}`;
    el.addEventListener('click', () => this.terminal.run(command));
    el.classList.add('link');
    el.innerText = command;
    return el;
  }

  formatArray(array) {
    if(array.length === 0) return this.formatScalar('null', 'none');

    const div = document.createElement('div');
    div.classList.add('array');
    div.classList.add('collection');
    for(const item of array) {
      const container = document.createElement('div');
      container.appendChild(this.formatScalar('list-bullet', '- '));
      container.appendChild(this.format(item));
      div.appendChild(container);
    }
    return div;
  }

  formatObject(object) {
    const div = document.createElement('div');
    div.classList.add('object');
    div.classList.add('collection');
    for(const [key, value] of Object.entries(object)) {
      const container = document.createElement('div');
      container.appendChild(this.formatScalar('object-key', key));
      container.appendChild(this.formatScalar('object-colon', ': '));
      container.appendChild(this.format(value, key));
      div.appendChild(container);
    }
    return div;
  }

  async promptSecret(message='', namespace='default') {
    this.prompt = new APITerminalSecretPrompt(message, namespace);
    this.el.appendChild(this.prompt.el);
    this.prompt.input.focus();
    return await this.prompt.input.value;
  }

  async promptInput(message='') {
    this.prompt = new APITerminalCommandPrompt(message);
    this.el.appendChild(this.prompt.el);
    this.prompt.input.focus();
    return await this.prompt.input.value;
  }
}

class APITerminalPrompt {
  constructor() {
    this.el = document.createElement('div');
    this.el.classList.add('terminal-prompt');
    this._click = (e) => this.click(e);
    this.el.addEventListener('click', this._click);
  }

  click(e) {
    if(e.target === this.input.el) return;
    e.preventDefault();
    this.input.focus();
  }
}

class APITerminalCommandPrompt extends APITerminalPrompt {
  constructor(prefix='') {
    super();
    this.prefix = document.createElement('span');
    this.prefix.classList.add('terminal-prompt-prefix');
    this.prefix.innerText = prefix;
    this.el.appendChild(this.prefix);
    this.input = new APITerminalCommandInput();
    this.el.appendChild(this.input.el);
  }
}

class APITerminalSecretPrompt extends APITerminalPrompt {
  constructor(message='', namespace='default') {
    super();
    this.message = document.createElement('span');
    this.message.classList.add('terminal-prompt-prefix');
    this.message.innerText = message;
    this.el.appendChild(this.message);
    this.input = new APITerminalSecretInput(namespace);
    this.el.appendChild(this.input.el);
  }
}

class APITerminalInput {
  constructor() {
    this.value = new Promise((resolve) => { this.submit = resolve; });
    this.eventListeners = {};
    this.build();
    this.bindKeypresses();
  }

  build() {
    // Abstract interface
  }

  set(input) {
    // Abstract interface
  }

  get() {
    // Abstract interface
  }

  bindKeypresses() {
    this._keypress = (e) => this.keypress(e);
    this._keydown = (e) => this.keydown(e);
    this.el.addEventListener('keypress', this._keypress);
    this.el.addEventListener('keydown', this._keydown);
  }

  on(event, listener) {
    this.eventListeners[event] = this.eventListeners[event] || [];
    this.eventListeners[event].push(listener);
  }

  emit(event, data) {
    for(const listener of this.eventListeners[event] || []) {
      listener(data);
    }
  }

  teardown() {
    this.el.removeEventListener('keypress', this._keypress);
    this.el.removeEventListener('keydown', this._keydown);
    this.eventListeners = {};
  }

  keypress(e) {
    // Enter
    if(e.keyCode == 13) {
      e.preventDefault();
      this.submit(this.get());
      this.teardown();
      return;
    }

    this.emit('keypress', e);
  }

  keydown(e) {
    if(e.keyCode == 38) {
      e.preventDefault();
      this.emit('up');
    }
    if(e.keyCode == 40) {
      e.preventDefault();
      this.emit('down');
    }
  }

  focus() {
    const text = this.el.childNodes[0];
    if(text) {
      const range = document.createRange();
      range.setStart(text, text.length);
      range.collapse(true);
      const selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
    }
    this.el.focus();
    this.emit('focus');
  }
}

class APITerminalCommandInput extends APITerminalInput {
  build() {
    this.el = document.createElement('span');
    this.el.classList.add('terminal-input');
    this.el.contentEditable = true;
  }

  teardown() {
    super.teardown();
    this.el.contentEditable = false;
  }

  set(input) {
    this.el.innerText = input;
    this.focus();
  }

  get() {
    return this.el.innerText;
  }
}

class APITerminalSecretInput extends APITerminalInput {
  constructor(namespace) {
    super();
    this.el.name = namespace;
  }

  build() {
    this.el = document.createElement('input');
    this.el.classList.add('terminal-input', 'terminal-secret-input');
    this.el.type = 'password';
  }

  teardown() {
    super.teardown();
    this.el.disabled = true;
  }

  set(input) {
    this.el.value = input;
    this.focus();
  }

  get() {
    return this.el.value;
  }
}

const terminal = new APITerminal(document.querySelector('.terminal'), 'http://127.0.0.1:4000');

(async () => {
  const hash = window.location.hash.slice(1);
  let method = 'GET';
  let path = '/';
  let args = '';
  if(hash) {
    const hashParts = hash.split(/[:?&]/g);
    method = hashParts[0];
    path = hashParts[1];
    args = hashParts.slice(2).join(' ');
    const commands = {
      GET: 'get',
      POST: 'post',
    };
    const command = `${commands[method]} ${path} ${args}`;
    if(method === 'GET') {
      terminal.run(command);
    } else {
      terminal.context.prompt.input.set(command);
    }
  } else {
    terminal.run('help');
    await terminal.runningContext;
    terminal.run('ls');
  }
})();
